FROM php:8.1-apache as apache

RUN apt update -y \
    && apt install pip git zip -y \
    && apt-get install -y wget gnupg

# ------- INSTALL PHP EXTENSIONS ------- 
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions exif sodium pdo_mysql zip intl
# ------- INSTALL PHP EXTENSIONS ------- 


COPY ./apache/default.conf /etc/apache2/sites-available/000-default.conf

FROM apache AS dev
# TODO


FROM apache AS prod
# TODO
