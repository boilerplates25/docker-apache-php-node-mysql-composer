# Boilterplate for dockerize PHP / Node / MySQL app all in one

## Prerequisites
- Install of docker and docker compose

That's all !

---

## Start project
```bash
docker-compose up --build -d
```
Or for newest version :
```bash
docker compose up --build -d
```

---


## Install Composer / Node packages
### Composer

#### Install all dependencies
```bash
docker run --rm -it -w /app -v $PWD/symfony_project/:/app composer:latest composer install
```

#### Install specific dependencie
```bash
docker run --rm -it -w /app -v $PWD/symfony_project/:/app composer:latest composer req example-package
```

---


### Node

#### Install all dependencies
```bash
docker run --rm -it -w /app -v $PWD/symfony_project/:/app node:latest npm install
```
OR
```bash
docker run --rm -it -w /app -v $PWD/symfony_project/:/app node:latest yarn install
```

#### Install specific dependencie
```bash
docker run --rm -it -w /app -v $PWD/symfony_project/:/app node:latest npm install example-package
```
OR
```bash
docker run --rm -it -w /app -v $PWD/symfony_project/:/app node:latest yarn install example-package
```

## Start PHP / Node server
### PHP

PHP server is automaticly started by
```bash
docker compose up --build -d
```

### Node
```bash
docker run --rm -it -w /app -v $PWD/symfony_project/:/app node:latest npm run dev
```

OR

```bash
docker run --rm -it -w /app -v $PWD/symfony_project/:/app node:latest npm run watch
```